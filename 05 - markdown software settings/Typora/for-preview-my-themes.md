---
title: Lorem ipsum
author: dummy
description: this block is YAML front matters
---

# H1 標題

In [publishing](https://www.wikiwand.com/en/Publishing) and [graphic design](https://www.wikiwand.com/en/Graphic_design), **lorem ipsum** (derived from Latin *dolorem ipsum*, translated as "pain itself") is a [filler text](https://www.wikiwand.com/en/Filler_text) commonly used to demonstrate the graphic elements of a document or visual presentation. [^1]

**The Toyota Visual Identity System (VIS)** is a comprehensive tool to *guide* communications of both the Toyota brand and its products in a unified and consistent manner. Here you’ll find information and guidance on the various design elements that make up ~~our visual identity~~.

## H2 標題

A common form of *lorem ipsum* reads:
> **The Toyota Visual Identity System (VIS)** is a comprehensive tool to *guide* communications of both the Toyota brand and its products in a unified and consistent manner. Here you’ll find information and guidance on the various design elements that make up ~~our visual identity~~.

Inline styles support **strong**, *Emphasis*, `code`, <u>underline</u>, ~~strikethrough~~, :haha:, $\LaTeX$, X^2^, H~2~O, ==highlight==, [Link](typora.io), and image:

![img](https://images.unsplash.com/photo-1504198266287-1659872e6590?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60)

----

Block level contains:

### Heading 3 標題

#### Heading 4 標題

##### Heading 5 標題

###### Heading 6 標題 .subtitle-2

| Left-Aligned  | Center Aligned  | Right Aligned |
| :------------ | :-------------: | ------------: |
| col 3 is      | some wordy text |         $1600 |
| col 2 is      |    centered     |           $12 |
| zebra stripes |    are neat     |            $1 |


+ [x] something is DONE.
+ [ ] something is not TODO. (THIS LINE IS FOCUSED!)
	
1. ordered list item 1.
2. ordered list item 2.
   + sub-unordered list item 1.
   + sub-unordered list item 2.
     + [x] something is DONE.
     + [x] something is not something is not something is not something is not something is not something is not something is not something is not TODO. (THIS LINE IS FOCUSED!)
	
- 以上活動適用對象限於自即日起訂購且於本月底前完成領牌手續者
- 和泰汽車保留以上活動內容變更及最終解釋之權利，變更或修改訊息將於TOYOTA官網公布，不另行通知。
  -  分期付款與現金付款之建議零售價相同，但實際交易價格由買賣雙方各自依交易條件內容協商議定之。 
  -  本專案為6年期(72個月)利率5%且適用車款為VIOS、YARIS，經銷商及指定貸款企業保有審核之權利 。 
-  活動限制為：各車款0利率優惠方案不同，且經銷商及指定貸款企業保有審核之權利。零利率購車優惠方案之分期付款與現金付款之建議零售價相同，但實際交易價格由買賣雙方各自依交易條件內容協商議定之。

```html
<!DOCTYPE html>
<html>
<body>

<h1>The *= Operator</h1>
  
<p id="demo"></p>

<script>
var x = 10;
x *= 5;
document.getElementById("demo").innerHTML = x;
</script>

</body>
</html>
```

[TOC]

[^1]: *Forked* from https://en.wikipedia.org/wiki/Lorem_ipsum